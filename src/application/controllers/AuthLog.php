<?php

defined('BASEPATH') OR exit('No direct script access allowed');

define("NAO_LOGADO", "NAO_LOGADO", true);


/**
 * Classe responsável por verificar se o usuário está logado. 
 * Permitindo o acesso apenas mediante login.
 */
class AuthLog extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('frontend_helper'); 
        $this->load->model('Auth_model', 'auth_model');
        if(!$this->isLogged()){
            if(isset($_SESSION[NAO_LOGADO])&&
                $_SESSION[NAO_LOGADO] != get_class()){
                redirect(base_url(LOGIN_URL));
                exit();
            }else{
                $this->session->set_flashdata(NAO_LOGADO, get_class($this));    
            }
        }
    }
    
    /**
     * Função que retorna o HTML padrão do controller
     * Formulário de login
     */
    public function index(){
        $data['mensagens'] = mensagens();
        
        $this->load->view('header');
        $this->load->view('login', $data);
        $this->load->view('footer');
    }

    /**
     * Método responsável por validar os dados inseridos pelo usuário
     * e verificar se a combinação inserida está armazenada no banco
     */
    public function authenticate(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules(
            'email',
            'Email', 
            'required|valid_email'
        );
        $this->form_validation->set_rules(
            'senha', 
            'Senha', 
            'required|min_length[4]', 
            array('required' => 'Você deve preencher a %s.')
        );

        $email = $this->input->post('email');
        $senha = $this->input->post('senha');

        if(!$this->form_validation->run()){
            $this->session->set_flashdata('danger', validation_errors());
            $this->index();
        }else{
            if($this->authenticateLogin($email, $senha)){
                redirect(base_url('Admin'));
            }else{
                $this->index();
            }
        }
    }

    /**
     * Método responsável por verificar se a senha inserida corresponde
     * ao hash armazenado
     * @param  String $email    Login
     * @param  String $password Senha
     * @return boolean          Retorna verdadeiro caso correspondam.
     */
    private function authenticateLogin($email, $password){
        $passwordHash = $this->auth_model->getPasswordHash($email);
        if($email && password_verify($password, $passwordHash)){
            $this->session->set_userdata(
                'usuario', 
                $email
            );
            return TRUE;
        }else{
            exit();
            $this->session->sess_destroy();           
            $this->session->set_flashdata(
                'danger', 
                'E-mail ou Senha incorretos'
            );
            return FALSE;
        }
    }

    /**
     * Método responsável por verificar se o usuário está logado
     * @return boolean Verdadeiro caso logado.
     */
    private function isLogged(){
        return $this->session->has_userdata('usuario');
    }
}