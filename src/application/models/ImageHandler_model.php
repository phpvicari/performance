<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ImageHandler_model extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function loadaImagens($tabela, $coluna_id, $id, $coluna_imagem = "id_imagem"){
		$this->db->select('imagem.nome');
		$this->db->join('imagem', "imagem.id = $coluna_imagem", 'INNER');
		$this->db->where("$tabela.$coluna_id", $id);
		$imagens = $this->db->get($tabela)->result();
		if(!$imagens){
			return array('default.jpg');
		}
		return $imagens;
	}

}