<div id="homebody">

	<div class="page-header">
		<h1><?= $verbose_name ?> <small>novo</small></h1>

		<a href="<?= base_url($controller) ?>" class="btn btn-info"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Voltar</a>
		
	</div>





	<div class="col-md-8">
		<form action="<?= base_url($controller.'/save/') ?>" id="save_form" method="post" accept-charset="utf-8">
			<?php
				for($i = 0; $i < count($attributes); $i++){
			?>
					<div class="col-md-<?= $attributes[$i]['bootstrap_class'] ?>">
						<label><?= $attributes[$i]['label'] ?></label>
						<input type="<?= $attributes[$i]['type'] ?>" name="<?= $attributes[$i]['name'] ?>"  required="<?= $attributes[$i]['required'] ?>" class="form-control" id="<?= $attributes[$i]['name'] ?>"  />
					</div>
			<?php
				} 
			?>
			<div class='col-md-12'><br>
				<button class='btn btn-info form-control'> Cadastrar</button>
			</div>
			
		</form>
	</div>

</div>
</div>